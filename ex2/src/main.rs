#[allow(unused_imports)]
use failure::Error;

use ::ndarray as nd;
#[allow(unused_imports)] // Traits
use nd::prelude::{Dimension, NdFloat, ShapeBuilder, AsArray, }; // Traits
#[allow(unused_imports)] // Other usefull things
use nd::{s}; // slice syntax macro

#[allow(unused_imports)]
use rand::{thread_rng, seq::SliceRandom, Rng, rngs::StdRng, SeedableRng};
//use std::time::Instant;


#[allow(dead_code)]
const DEBUG: bool = false;

#[derive(Copy, Clone, PartialEq, Debug)]
enum BoardValue{
    X,
    O,
    Empty,
}

impl BoardValue {
    fn symbol_as_string(&self) -> String {
        match self {
            BoardValue::X => String::from("x"),
            BoardValue::O => String::from("o"),
            BoardValue::Empty=> String::from(" "),
        }
    }

    // TODO how to swap enum value inside member function?
}

#[derive(Clone)]
pub struct TicTacToeGrid {
    turn: BoardValue, // Player turn.  If BoardValue::empty, then game is over
    number_moves_left: u32,
    board: nd::Array2<BoardValue>,
    last_inserted_postion: Option<(usize, usize)>,
    straight_len: usize,
}

impl TicTacToeGrid{
    fn new(side_length: u32, row_len: u32) -> TicTacToeGrid {
        return TicTacToeGrid{
            turn: BoardValue::X,
            number_moves_left: side_length.pow(2),
            board: nd::Array2::<BoardValue>::from_elem(
                (side_length as usize, side_length as usize),
                BoardValue::Empty),
            last_inserted_postion: None,
            straight_len: row_len as usize,
        };
    }

    fn place_value(&mut self, y: usize, x: usize, val: BoardValue) -> bool{
        assert!(val == self.turn);
        match self.board[[y,x]] {
            BoardValue::Empty => {self.board[[y,x]] = val;},
            _ => return false,
        }

        self.last_inserted_postion = Some((y, x));
        self.number_moves_left -= 1;

        let ended = self.check_straights();

        if (self.number_moves_left == 0) && !ended {
            self.turn = BoardValue::Empty;
        }
        else if !ended {
            self.swap_player_turn();
        }
        return true;
    }

    fn swap_player_turn(&mut self) {
        match self.turn{
            BoardValue::X => {self.turn = BoardValue::O;},
            BoardValue::O => {self.turn = BoardValue::X;},
            _ => {assert!(false);},
        }
    }

    fn print(&self) {
        print!("–");
        for _i in 0..(self.board.shape()[1]+1){
            print!("––");
        }
        println!("");
        for (i, row) in self.board.outer_iter().enumerate(){
            print!("| ");
            for (j, val) in row.iter().enumerate(){
                let mut val = val.symbol_as_string();
                if let Some(v) = self.last_inserted_postion {
                    if v == (i,j) { val = val.to_uppercase(); }
                }
                print!("{:1} ", val);
            }
            println!("|")
        }
    }

    fn check_straights(&self) -> bool {
        fn check(arr: nd::ArrayView1<BoardValue>, straight_len: usize) -> bool {
            if arr.len() == 0 { return false; };
            let mut old = arr[0];
            let mut counter = 1;

            for &val in arr.iter().skip(1){
                if (val == old) && (val != BoardValue::Empty) {
                    counter += 1;
                    if counter >= straight_len {
                        return true;
                    }
                } else {
                    old = val;
                    counter = 0;
                }
            }
            return false;
        }

        struct DiagonalGenerator<'a>{
            //board: &nd::Array2<BoardValue>,
            board: &'a nd::Array2<BoardValue>,
            count: usize,
        }

        #[derive(Clone, Copy, Debug, PartialEq)]
        enum Pos {
            Up,
            Right,
            Down,
            Left,
        }

        impl<'a> DiagonalGenerator<'a> {
            fn get_index_on_outer_edge(counter: usize, side_len: usize)
                -> (usize, usize, Pos, usize) {
                fn outer_edge_1d_projection_idx(counter: usize, side_len: usize)
                    -> (usize, Pos, usize) {
                    if counter < side_len-1 {
                        return (counter,
                                Pos::Up,
                                counter+1);
                    } else if (counter >= side_len-1) && (counter < 2*side_len-2) {
                        return (side_len - 1,
                                Pos::Right,
                                2*side_len-1 - counter);
                    } else if (counter >= 2*side_len-2) && (counter < 3*side_len-3) {
                        return (side_len-1 - counter % (2*side_len-2),
                                Pos::Down,
                                counter - (2*side_len-2) + 1);
                    } else if (counter >= 3*side_len-3) && (counter < 4*side_len-4) {
                        return (0,
                                Pos::Left,
                                4*side_len-3 - counter);
                    } else {
                        assert!(false);
                        return (0, Pos::Down, 0);  // Pleasing rust compiler...
                    }
                }
                assert!(counter < 4*side_len-4);
                let (x_idx, pos, diag_len) = outer_edge_1d_projection_idx(
                    counter, side_len);
                let (y_idx, _, _)= outer_edge_1d_projection_idx(
                    (counter + (3*side_len -3)) % (4*side_len -4), side_len);
                return (x_idx, y_idx, pos, diag_len);
            }
        }

        impl<'a> Iterator for DiagonalGenerator<'a> {
            type Item = nd::Array1<BoardValue>;
            fn next(&mut self) -> Option<Self::Item> {
                let side_len = self.board.shape()[0];
                if self.count >= 4*side_len - 4 {
                    return None;
                }

                let (x_idx, y_idx, pos, diag_len) = Self::get_index_on_outer_edge(
                    self.count, side_len);

                let mut arr;

                if (pos == Pos::Up) || (pos == Pos::Right) {
                    arr = nd::Array1::from_elem(diag_len, BoardValue::Empty);
                    for (i, val) in arr.iter_mut().enumerate() {
                        *val = self.board[[y_idx+i, x_idx-i]];
                        //| dbg_arr[[y_idx+i, x_idx-i]] = i+1;
                    }
                }
                else {
                    let (x_idx, y_idx, _pos, _diag_len) = Self::get_index_on_outer_edge(
                        self.count - (side_len-1), side_len);
                    arr = nd::Array1::from_elem(diag_len, BoardValue::Empty);
                    for (i, val) in arr.iter_mut().enumerate() {
                        *val = self.board[[y_idx-i, x_idx-i]];
                        //| dbg_arr[[y_idx-i, x_idx-i]] = i+1;
                    }
                }

                self.count += 1;
                return Some(arr);
            }
        }

        // Rows & Columns
        for dim in 0..=1 {
            for (_i, row) in self.board.axis_iter(nd::Axis(dim)).enumerate(){
                let found = check(row, self.straight_len);
                if found { return true; }
            }
        }

        // Diagonals
        let diag_gen = DiagonalGenerator{board: &self.board, count: 0};
        for diag in diag_gen {
            //dbg!(&diag);
            let found = check(diag.view(), self.straight_len);
            if found { return true; }
        }
        return false;
    }
}


/// TicTacToe
mod bots{
    use super::*;
    use std::io::Write;

    ///use itertools::Itertools;
    #[allow(dead_code)]
    pub fn naive_move(game: &mut TicTacToeGrid){
        assert!(game.number_moves_left > 0);


        let mut insert_pos: Option<(usize, usize)> = None;

        for (i, row) in game.board.outer_iter().enumerate(){
            assert!(row.ndim() == 1);
            assert!(row.shape().len() == 1);

            for (j, cell) in row.iter().enumerate(){
                match cell {
                    BoardValue::Empty => { insert_pos = Some((i, j)); break;}
                    _ => {continue;}
                }
            }
            if let Some(_v) = insert_pos{
                break;
            }
        }
        if insert_pos == None {
            assert!(false);
        }
        let p = insert_pos.unwrap();

        game.place_value(p.0, p.1, game.turn);
    }

    #[allow(dead_code)]
    pub fn random_move(game: &mut TicTacToeGrid){
        assert!(game.number_moves_left > 0);


        let mut insert_pos: Option<(usize, usize)> = None;
        let side_len = game.board.shape()[0];

        let rng2: &mut StdRng = &mut SeedableRng::seed_from_u64(thread_rng().gen::<u64>());
        let mut trials = (0..(side_len)).collect::<Vec<usize>>();
        trials.shuffle(rng2);
        let trials = nd::Array::from_vec(trials);

        for (i, row) in game.board.outer_iter().enumerate(){
            assert!(row.ndim() == 1);
            assert!(row.shape().len() == 1);

            for (j, cell) in row.iter().enumerate(){
                match cell {
                    BoardValue::Empty => { insert_pos = Some((trials[i], trials[j])); break;}
                    _ => {continue;}
                }
            }
            if let Some(_v) = insert_pos{
                break;
            }
        }
        if insert_pos == None {
            assert!(false);
        }
        let p = insert_pos.unwrap();

        game.place_value(p.0, p.1, game.turn);
    }

    #[allow(dead_code)]
    pub fn human_move(game: &mut TicTacToeGrid){
        use std::io;
        print!("Make your move, player {}. ", game.turn.symbol_as_string());

         loop {
            print!("Give x and y: ");
            io::stdout().flush().ok().expect("Could not flush stdout");
            let mut guess = String::new();
            io::stdin().read_line(&mut guess).expect("Failed to read line");
            let spl = guess.split_whitespace().map(|s| s.to_string()).collect::<Vec<String>>();
            if spl.len() != 2 {
                println!("Wrong number of values");
                continue
            }
            let mut nums = Vec::<usize>::with_capacity(2);
            for s in spl {
                match s.trim().parse::<usize>() {
                    Ok(v) => {nums.push(v);},
                    Err(_e) => {println!("Please type a number!"); continue;}
                }
            }
            let (x, y) = (nums[0], nums[1]);
            match game.place_value(x, y, game.turn) {
                true => break,
                false => println!("Illegal position you jerk!")
            }
        }
    }

    #[allow(non_snake_case)]
    pub fn min_max_DFS_move(game: &mut TicTacToeGrid) {
        let mut trial = game.clone();
        let (_winner_at_min, _min_dep, move_at_min)
                = min_max_recursive(&mut trial, 0);

        let (y, x) = move_at_min;
        game.place_value(y, x, game.turn);
    }

    fn min_max_recursive(trial: &mut TicTacToeGrid, depth: u32)
        -> (BoardValue, u32, (usize, usize)) {

        // End condition
        if trial.check_straights() {
            return (trial.turn, depth, (100000, 100000));
        }
        if trial.number_moves_left == 0 {
            return (BoardValue::Empty, depth, (100000, 100000));
        }

        let mut winners = nd::Array1::<BoardValue>::from_elem(
            trial.number_moves_left as usize, BoardValue::Empty);
        let mut depths = nd::Array1::<u32>::from_elem(
            trial.number_moves_left as usize, 100000);
        let mut moves = nd::Array1::<(usize,usize)>::from_elem(
            trial.number_moves_left as usize, (100000, 100000));
        let mut counter: usize = 0;

        // iterate all possible moves
        for i in 0..trial.board.shape()[0]{
            for j in 0..trial.board.shape()[1]{
                if trial.board[[i,j]] != BoardValue::Empty {
                    continue;
                }
                let turn = trial.turn;
                let num_moves = trial.number_moves_left;
                // Do move
                trial.place_value(i,j, turn);
                let (winner, dep, _) = min_max_recursive(
                    trial, depth+1);
                // Restore original state
                trial.board[[i,j]] = BoardValue::Empty;
                trial.turn = turn;
                trial.number_moves_left = num_moves;

                winners[counter] = winner;
                depths[counter] = dep;
                moves[counter] = (i,j);
                counter += 1;
            }
        }
        let mut min_dep = 100000;
        let mut winner_at_min = None;
        let mut move_at_min = None;

        for ((&winner, &dep), &mov) in winners.iter().zip(depths.iter())
                .zip(moves.iter()) {
            if (winner == trial.turn) && (winner != BoardValue::Empty) {
                if dep < min_dep {
                    min_dep = dep;
                    winner_at_min = Some(winner);
                    move_at_min = Some(mov);
                }
            }
        }
        // Winning was not possible, next best is tie
        if min_dep == 100000 {
            for ((&winner, &dep), &mov) in winners.iter().zip(depths.iter())
                .zip(moves.iter()) {
                if winner == BoardValue::Empty {
                    if dep < min_dep {
                        min_dep = dep;
                        winner_at_min = Some(winner);
                        move_at_min = Some(mov);
                    }
                }
            }
        }
        // Ugh we have to lose :(
        if min_dep == 100000 {
            for ((&winner, &dep), &mov) in winners.iter().zip(depths.iter())
                .zip(moves.iter()) {
                if !(winner == BoardValue::Empty) && !(winner == trial.turn) {
                    if dep < min_dep {
                        min_dep = dep;
                        winner_at_min = Some(winner);
                        move_at_min = Some(mov);
                    }
                }
            }
        }

        assert!(winner_at_min != None); // Some move should have been found

        return (winner_at_min.unwrap(), min_dep, move_at_min.unwrap());
    }
}




fn prob1() {
    println!(
        "1.Define the problem and describe the search space of tic-tac-toe. What is the
branching factor in tic-tac-toe?\n");

    println!(
        "Problem is to get 3 own symbols in row (vertical, horizontal, diagonal) in
3x3 grid. Branching factor is from 9 to 1.");
}

fn prob2(){
    let mut game = TicTacToeGrid::new(3, 3);
    while game.number_moves_left > 0 {

        if game.turn == BoardValue::X{
            //bots::naive_move(&mut game);
            bots::min_max_DFS_move(&mut game);
        } else if game.turn == BoardValue::O {
            bots::random_move(&mut game);
            //bots::human_move(&mut game);
            //bots::naive_move(&mut game);
        } else {
            break;
        }

        game.print();
        let end = game.check_straights();
        if end {
            println!("Game ended! Winner: {}", game.turn.symbol_as_string());
            break;
        }
        else if game.number_moves_left == 0 {
            println!("Game ended! Tie!");
            break;
        }
    }
}

fn prob3(){
    println!(
"3.Which AI techniques does your program utilize?\n");

    println!(
"Depth First search. The game is solved.");
}

fn prob4(){
    println!(
"4.5.How well does your program fare against a randomly playing adversary?\n");

    println!(
"Wins. Human player gets to tie.");
    let mut wins = 0;
    let mut ties = 0;
    let total = 200;
    for i in 0..total {
        let mut game = TicTacToeGrid::new(3, 3);
        let first_move = game.turn;
        while game.number_moves_left > 0 {
            if game.turn == BoardValue::X {
                bots::min_max_DFS_move(&mut game);
            } else if game.turn == BoardValue::O {
                bots::random_move(&mut game);
            } else {
                break;
            }

            // game.print();
            let end = game.check_straights();
            if end {
                // println!("Game ended! Winner: {}", game.turn.symbol_as_string());
                if game.turn == first_move && game.turn != BoardValue::Empty {
                    wins += 1;
                }
                else if game.turn == BoardValue::Empty {
                    ties += 1;
                }
                break;
            }
        }
        if i%100 == 0 {println!("{} / {}", i, total);}
    }
    println!("Wins {}, ties {} out of {}\n\n", wins, ties, total)
}

fn prob5(){
    println!(
"5.What changes if we examine a generalized version of tic-tac-toe where there
is an unbounded grid of cells and the aim is to get five of own marks in a row.
Is your program still valid? If not, how could you fix it?\n");

    println!(
"Way too high branching factor. It is coded here, but it takes a forever. To get over this I should add max depth parameter and some 'game state indicator'.");
}

fn prob6(){
    println!(
"6.Are there naturally arising heuristic functions that can be used here?\n");

    println!(
"If we believe my bot, start from corner.");
}


fn main() {
    prob1();
    prob2();
    prob3();
    prob4();
    prob5();
    prob6();
}
