#[allow(unused_imports)]
use failure::Error;

use ndarray as nd;
#[allow(unused_imports)] // Traits
use nd::prelude::{Dimension, NdFloat, ShapeBuilder, AsArray, };
use nd::{s};  // slice syntax

use rand::{thread_rng, seq::SliceRandom, Rng, rngs::StdRng, SeedableRng};
use std::collections::{HashSet, HashMap};

const DEBUG: bool = false;

#[derive(Clone)]
pub struct Sudoku {
    // Below it can be e.g. n = 3
    n: usize,
    /// Filled sudoku board. Shape (n*n)*(n*n)
    board_solution: nd::Array4<i32>,
    /// Sudoku board with numbers removed. If value some is <= 0 then it's empty
    board_partially_filled: nd::Array4<i32>,
    /// Domain of possible numbers for each cell. This can be used by solving
    /// algorithms. Array is shape of (n*n)*(n*n) * (n^2)
    domain: nd::Array4<nd::Array1<bool>>,
    /// Arcs from cell index to another index
    arcs: HashMap<(usize, usize, usize, usize),
                  HashSet<(usize, usize, usize, usize)>>,
}

/// Previous implementation from problem 1: back propagation
impl Sudoku {
    pub fn new(n: usize, hardness: i32, seed: u64) -> Sudoku {
        assert!(hardness >= 1 && hardness <= 5);
        assert!(n >= 2);

        let mut solution = nd::Array4::<i32>::zeros((n, n, n, n));
        let mut rng = SeedableRng::seed_from_u64(seed as u64);

        // fill full board
        let _ = Sudoku::recursive_solution(0, &mut solution, true, true, Some(&mut rng));

        // Initialize partially filled board as full board and then remove numbers
        let mut partially_filled = solution.clone();

        // Remove values from filled one
        Sudoku::remove_numbers(hardness-1,&mut partially_filled, &mut rng);

        let mut sudoku = Sudoku {
            n: n,
            board_solution: solution,
            board_partially_filled: partially_filled,
            domain: nd::Array4::from_elem((n,n,n,n),
                                          nd::Array1::from_elem(n.pow(2), true)),
            arcs: HashMap::new(),
        };

        sudoku.init_costraints();

        return sudoku;
    }

    pub fn get_solution_board(&self) -> nd::Array4<i32>{
        return self.board_solution.clone();
    }

    pub fn get_partially_filled_board(&self) -> nd::Array4<i32>{
        return self.board_partially_filled.clone();
    }

    fn remove_numbers(hardness: i32, mut board: &mut nd::Array4<i32>,
                      rng: &mut StdRng){
        let n = board.dim().0;
        let mut threshold = 2_i32.pow(hardness as u32);
        while threshold > 0 {
            let mut indices = (9,9,9,9);
            let mut orig= 0;
            while orig < 1{
                let random_index = rng.gen_range(0, n.pow(4));
                indices = Sudoku::to_flat_itr(random_index, n);
                orig = board[indices];
            }
            board[indices] = 0;
            let count = Sudoku::recursive_solution(0, &mut board, false, false, Some(rng));

            assert!(count > 0);
            if count > 1 {
                if DEBUG{
                    println!("Value removing progress {}/{}",
                             threshold, 2_i32.pow(hardness as u32));
                }
                threshold -= 1;
                board[indices] = orig;
            }
        }
    }

    fn to_flat_itr(flat_itr: usize, n: usize) -> (usize, usize, usize, usize) {
        let i = flat_itr / n.pow(3);
        let j = (flat_itr % n.pow(3)) / n.pow(2);
        let k = (flat_itr % n.pow(2)) / n;
        let l = flat_itr % n;
        return (i,j,k,l);
    }

    pub fn test_direct_validity_of_placement(board: &nd::Array4<i32>,
                                         num: i32, i: usize, j: usize,
                                         k: usize, l: usize) -> bool{

        assert!(board[[i, j, k, l]] < 1);

        // Column
        for &v in board.slice(s![..,j,..,l]).iter(){
            if v == num {
                return false;
            }
        }
        // Row
        for &v in board.slice(s![i,..,k,..]).iter(){
            if v == num {
                return false;
            }
        }
        // Block
        for &v in board.slice(s![i,j,..,..]).iter(){
            if v == num {
                return false;
            }
        }
        return true;
    }

    fn debug_board(board: &nd::Array4<i32>, debug: bool, i: usize,
                       j: usize, k: usize, l: usize){
        for (_j, a3) in board.axis_iter(nd::Axis(1)).enumerate(){
            for (_l, a2) in a3.axis_iter(nd::Axis(2)).enumerate(){
                for (_i, a1) in a2.outer_iter().enumerate(){
                    for (_k, &a0) in a1.iter().enumerate(){
                        if debug && (i,j,k,l) == (_i,_j,_k,_l){
                            print!("{:2}#", a0);
                        }
                        else if a0 < 1{
                            print!("   ");
                        }
                        else {
                            print!("{:2} ", a0);
                        }
                    }
                    print!("| ");
                }
                println!("");
            }
            for (_i, a3) in board.axis_iter(nd::Axis(1)).enumerate() {
                for (_j, _a2) in a3.axis_iter(nd::Axis(2)).enumerate(){
                    print!("–––");
                }
                print!(" –");
            }
            println!("");
        }
        println!("");
    }

    pub fn print_board(board: &nd::Array4<i32>){
        Sudoku::debug_board(&board, false, 0, 0, 0, 0);
    }

    pub fn recursive_solution(flat_itr: usize, mut board: &mut nd::Array4<i32>,
                          leave_filled: bool, short_circuit: bool,
                          rng: Option<&mut StdRng>) -> usize{
        // Initialize stuff
        assert!(board.dim().0 == board.dim().1);
        assert!(board.dim().1 == board.dim().2);
        assert!(board.dim().2 == board.dim().3);
        assert!(!(!leave_filled && short_circuit));
        let n = board.dim().0;
        let mut tmp;
        // let a: &mut StdRng = &mut SeedableRng::seed_from_u64(thread_rng().gen::<u64>());
        let rng2: &mut StdRng =
            match rng{
                Some(v) => v ,
                None => {
                    tmp = SeedableRng::seed_from_u64(thread_rng().gen::<u64>());
                    &mut tmp
                },
            };

        let (i,j,k,l) = Sudoku::to_flat_itr(flat_itr, n);

        // End condition
        if flat_itr == n.pow(4){
            return 1;
        }

        let mut count = 0;  // Number of solutions for current square

        let mut trials = (1..=(n.pow(2) as i32)).collect::<Vec<i32>>();
        trials.shuffle(rng2);
        let trials = nd::Array::from_vec(trials);

        // Value already filled
        if board[[i,j,k,l]] > 0{
            count += Sudoku::recursive_solution(flat_itr+1, &mut board,
                                        leave_filled, short_circuit, Some(rng2));
            return count;
        }
        else{
            for trial_idx in 0..(n.pow(2)){
                let trial = trials[trial_idx];
                // Test direct contradictions whether number can be placed here.
                match Sudoku::test_direct_validity_of_placement(&mut board,
                                                                     trial, i, j, k, l) {
                    false => {continue;},
                    true => {board[[i, j, k, l]] = trial;},
                }
                // Test indirectly whether this number causes zero solutions
                count += Sudoku::recursive_solution(flat_itr + 1, &mut board,
                          leave_filled, short_circuit, Some(rng2));
                if (count == 0) || (!leave_filled) {
                    board[[i,j,k,l]] = 0;
                }
                if (count != 0) && (leave_filled) {
                    return count
                }
            }
        }
        return count;  // Contradiction, some guess previously is wrong
    }

    fn init_costraints(&mut self){
        assert!(!self.domain.is_empty());
        for i in 0..self.n {
            for j in 0..self.n{
                for k in 0..self.n {
                    for l in 0..self.n {
                        // Fill hash set: arcs to same row, column and block
                        let key = (i, j, k, l);
                        let cell = self.board_partially_filled[key];
                        if cell > 0 {
                            for v in self.domain[key].iter_mut() {
                                *v = false;
                            }
                            self.domain[key][(cell-1) as usize] = true;
                        }
                        self.add_neighbour(key);
                    }
                }
            }
        }
    }

    fn add_neighbour(&mut self, key: (usize, usize, usize, usize)) {

        let a = &self.board_partially_filled;
        // Same n*n block
        for (b, _v) in a.slice(s![key.0, key.1, .., ..]).indexed_iter() {
            if b != (key.2, key.3){
                self.arcs.entry(key).or_insert(HashSet::new())
                        .insert((key.0, key.1, b.0, b.1));
            }
        }
        // Same row
        for (b, _v) in a.slice(s![key.0, .., key.2, ..]).indexed_iter() {
            if b.0 != key.1 {
                self.arcs.entry(key).or_insert(HashSet::new())
                        .insert((key.0, b.0, key.2, b.1));
            }
        }
        // Same column
        for (b, _v) in a.slice(s![.., key.1, .., key.3]).indexed_iter() {
            if b.0 != key.0 {
                self.arcs.entry(key).or_insert(HashSet::new())
                        .insert((b.0, key.1, b.1, key.3));
            }
        }
    }
}

/// Constrain satisfaction approach for sudoku.
impl Sudoku {
    /// Arc-consistency
    /// https://en.wikipedia.org/wiki/AC-3_algorithm
    #[allow(non_snake_case)]
    pub fn AC3(&self) -> Result<Sudoku, Error> {
        let mut sud = self.clone();
        // Initialization of constraints is already done by Sudoku::new()

        loop {
            let (&x, neighbours_x) = sud.arcs.iter_mut().next().unwrap();
            let &y = neighbours_x.iter().next().unwrap();
            assert!(neighbours_x.remove(&y));

            if neighbours_x.is_empty() { sud.arcs.remove(&x); }


            if sud.arc_reduce(x, y) {
                if !sud.domain[x].iter().any(|&b| b) {
                    return Err(failure::err_msg("No solutions!"));
                }
                else {
                    sud.add_neighbour(x);
                }
            }

            if sud.arcs.is_empty() { break; }
        }

        sud.solve_with_constraint_reduction();
        return Ok(sud);
    }

    // Even though this uses mutable pointer, array is not mutated
    #[allow(non_snake_case)]
    fn constraint_R2(board: &mut nd::Array4<i32>,
                     x: (usize, usize, usize, usize),
                     y: (usize, usize, usize, usize),
                     vx: i32, vy: i32) -> bool{
        let x_orig = board[x];
        let y_orig = board[y];
        board[x] = vx;
        board[y] = 0;
        let succ = Sudoku::test_direct_validity_of_placement(board, vy, y.0,
                                                         y.1, y.2, y.3);
        board[x] = x_orig;
        board[y] = y_orig;
        return succ;
    }

    fn arc_reduce(&mut self, x: (usize, usize, usize, usize),
                  y: (usize, usize, usize, usize)) -> bool {
        let mut change: bool = false;

        let domain_x = self.domain[x].iter().enumerate()
                .filter(|(_i, &b)| b).map(|(i ,_x)| i).collect::<Vec<_>>();
        let domain_y = self.domain[y].iter().enumerate()
                .filter(|(_i, &b)| b).map(|(i ,_x)| i).collect::<Vec<_>>();
        for vx_i in domain_x.iter(){
            // find a value vy in D(y) such that vx and vy satisfy the constraint R2(x, y)
            let mut found = false;
            for vy_i in domain_y.iter(){
                if Sudoku::constraint_R2(&mut self.board_partially_filled,
                                      x, y, (vx_i+1) as i32, (vy_i+1) as i32){
                    found = true;
                }
            }
            // if there is no such vy
            if !found {
                self.domain[x][*vx_i] = false;
                change = true;
            }
        }
        return change;
    }

    fn solve_with_constraint_reduction(&mut self) {
        for (d, v) in self.domain.indexed_iter_mut() {
            let domain_size = v.iter().fold(0, |acc, &b| if b {acc + 1} else {acc});
            if domain_size == 1 {
                let idx = v.iter().position(|&b| b).unwrap();
                self.board_partially_filled[d] = (idx+1) as i32;
            }
        }
    }
}




