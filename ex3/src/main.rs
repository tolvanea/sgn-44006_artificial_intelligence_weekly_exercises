#[allow(unused_imports)]
use failure::Error;

use ndarray as nd;
#[allow(unused_imports)] // Traits
use nd::{prelude::{Dimension, NdFloat, ShapeBuilder, AsArray, }, s};

#[allow(unused_imports)]
use rand::{thread_rng, seq::SliceRandom, Rng, rngs::StdRng, SeedableRng};
use std::time::Instant;

mod sudoku_from_prob1;
use sudoku_from_prob1::Sudoku;




fn main() {
    let seed = 42;
    let stars = 2;
    let sudoku = Sudoku::new(3, stars, seed);

    println!("Solution 3x3:");
    Sudoku::print_board(&sudoku.get_solution_board());

    println!("{} stars  3x3:", stars);
    Sudoku::print_board(&sudoku.get_partially_filled_board());
    println!("");

    let mut board = sudoku.get_partially_filled_board();

    // backtracking
    let now = Instant::now();
    let sol_count = Sudoku::recursive_solution(0, &mut board, true,
                                               false, None);
    assert!(sol_count == 1); // Be sure that there exists only one solution.
    let time_bt = now.elapsed().as_micros();

    // AC3
    let now = Instant::now();
    let solved_ac3 = sudoku.AC3().unwrap();
    let time_ac3 = now.elapsed().as_micros();


    println!("Solution took, bactracking: {} us, AC3: {} us", time_bt, time_ac3);
    println!("Ac3 is much much slower");
    println!("\nPartial solution by AC3:");
    Sudoku::print_board(&solved_ac3.get_partially_filled_board());
    println!("");
}
