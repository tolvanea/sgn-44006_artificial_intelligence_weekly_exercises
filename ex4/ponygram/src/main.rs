//                                            .-'(     ).
//                                      .''`>'    \___/  \
//                                    .' .'`      .....-----...
//                                   / .'  __  .-`             `.
//                                  / /   /  \`             .:'--:.
//                                 ( /   ( /  \               `-.__..-;
//                                  Y    | |   `-..__  .,            -
//                                 (     ( '.  \ ____`\ )`-._     _-`
//                                 |     '\   __/   __\' / `:``''`
//                                 (     .|\_  (   / .-| |'.|
//                                  \    |' / ,'\ ( (WW| \W)j
//                                   \__.|  |    \_\_`/   ``-.
//            .--''''````-.             |'  l            \__/
//           /             `.           |    `.  -,______.-'
//          /                `.________.|      `.   /
//         (         ,.--''>-',:       |'        | (
//         |        |     /   (_)     .|        ,'),-``''-.
//         |       .'    | ,;         |'       / ,'        `.
//        .|       |.    | (_)  ;,    '.      (.(            :
//        |'       '|    |     (_)      `'---'`  `.       `:`;
//        |         '.  / \        /           `:. ;        ':
//        |.          `.   |      /-,_______\   ' `     .-;  |
//        '|            \_/      /     |    |\   `----'`.' .'
//         |             )      /     |     | `--,    \`''`
//         '.           /      |      |     |   /      )
//           `--_____--'|      |      |      | (       |
//      `:._.`       '. |      |      |      |  \      |
//       '        .-.  )|       \     |       \  `.___/
//        `---;    ) )'  \_______)     \_______)
//          .:___-'
// https://gitlab.com/mattia.basaglia/ASCII-Pony/blob/master/rendered/txt/applejack.txt
// Applejack is the best pony, btw.


//! This program tries to classify ponies given text-form replique lines. Huge thanks for the data creator!:
//! https://www.kaggle.com/liury123/my-little-pony-transcript

#![allow(non_snake_case)]

use std::fs::File;
use std::io::{BufRead, BufReader, Result};
use std::collections::HashMap;
use permutation;
use rand::seq::SliceRandom;
use itertools::{izip};

/// Fields of csv file.
pub struct ReplyLine {
    title: String,  // episode title
    writer: String, // episode writer
    pony: String,   // pony who said that dialog
    dialog: String, // dialog that pony said
}

//#[derive(Default)]
pub struct Ngram<'a> {
    // TODO generics over n-length >= 2. ie Type T : arrayTrait
    map_names_to_nums: HashMap<String, usize>,
    map_nums_to_names: HashMap<usize, String>,
    map_nums_to_count: HashMap<usize, usize>,
    total_word_count: usize,                   // All words said by anyone
    ponys_line_count: HashMap<String, usize>,   // How many reply lines each pony has
    ponys_word_count: HashMap<String, usize>,   // How many said words each pony has
    pony_word_pairs: HashMap<String, HashMap<(usize, usize), usize>>,  // 2-gram counts
    pony_word_probs: HashMap<String, HashMap<usize, usize>>,  // 1-gram counts
    writers_line_count: HashMap<String, usize>, // How many reply lines each writer has
    writers_word_count: HashMap<String, usize>, // How many words said by anypony has writer written
    writer_word_pairs: HashMap<String, HashMap<(usize, usize), usize>>,  // 2-gram counts
    writer_word_probs: HashMap<String, HashMap<usize, usize>>,  // 1-gram counts
    lines_enc: Vec<Vec<usize>>,  // dialog that pony said by using vocabulary indices
    lines: &'a Vec<ReplyLine>,
}

fn parse_clean_dialog_csv() -> Result<Vec<ReplyLine>> {
    let mut all_dialogs = Vec::<ReplyLine>::new();
    all_dialogs.reserve(36861);

    let file = File::open("data/clean_dialog.csv")?;
    for (_i, line) in BufReader::new(file).lines()
            .enumerate()
            .skip(1) {  // header
        // Line without ":s at both ends
        let line = line?;
        let mut line_iter = line.chars();
        line_iter.next();
        line_iter.next_back();
        let line_stripped: String = line_iter.collect();
        let mut fields = line_stripped.split("\",\"")
                .map(|s| s.to_owned());
        //assert!(fields.len() == 4);
        all_dialogs.push(
            ReplyLine {
                title: fields.next().unwrap(),
                writer: fields.next().unwrap(),
                pony: fields.next().unwrap(),
                dialog: match fields.next() {
                    Some(v) => v,
                    None => {
                        // Line entry is "NA":
                        //println!("Something shitty with line {}", i+1);
                        continue;
                    },
                },
            });
        assert_eq!(fields.next(), None);
        //println!("{} - {:?}", i, fields);
        //if i >= 10 { break; }
    }
    return Ok(all_dialogs);
}


mod ngram_stuff {
    use super::*;
    pub fn create_ngram(all_dialogs: &Vec<ReplyLine>, print: bool) -> Ngram {
        let mut ngram = create_vocabulary(&all_dialogs);
        fill_ngram(all_dialogs, &mut ngram);

        if print { print_info(&ngram); }

        return ngram;
    }

    fn create_vocabulary(data: &Vec<ReplyLine>) -> Ngram {
        let mut map_names_to_nums: HashMap<String, usize> = HashMap::new();
        let mut map_nums_to_names: HashMap<usize, String> = HashMap::new();
        let mut map_nums_to_count: HashMap<usize, usize> = HashMap::new();
        let mut lines_encoded = vec![Vec::<usize>::new(); data.len()];

        map_names_to_nums.insert("UNKNOWN_WORD".to_string(), 0);
        map_nums_to_names.insert(0, "UNKNOWN_WORD".to_string());

        let mut word_count = 9;
        for (line, enc) in data.iter()
                .zip(lines_encoded.iter_mut()) {
            // Encrypt words to numbers
            for raw_word in line.dialog.split_whitespace() {
                let word_no_delimiter:String =
                        match raw_word.chars().rev().next() {
                            Some('.') | Some('?') | Some('!') | Some(',') => {
                                let mut itrt = raw_word.chars();
                                itrt.next_back();
                                itrt.map(|s| s.to_owned()).collect()
                            }, None => {
                                assert!(false, "Are there two sequential spaces?");
                                "a".to_string()
                            }, _ => raw_word.to_owned(),
                        };
                let word = word_no_delimiter.to_lowercase();
                if ! map_names_to_nums.contains_key(&word){
                    word_count += 1;
                    map_names_to_nums.insert(word.clone(), word_count);
                    map_nums_to_names.insert(word_count, word.clone());
                }
                let num = map_names_to_nums[&word];
                let c = map_nums_to_count.entry(num).or_insert(0);
                *c += 1;

                enc.push(num);
            }
        }

        let total_word_count = map_nums_to_count.values()
                .fold(0, |acc, &c| acc + c);

        let ngram = Ngram {
            map_names_to_nums,
            map_nums_to_names,
            map_nums_to_count,
            total_word_count: total_word_count,
            ponys_line_count: HashMap::new(),
            ponys_word_count: HashMap::new(),
            pony_word_pairs: HashMap::new(),
            pony_word_probs: HashMap::new(),
            writers_line_count: HashMap::new(),
            writers_word_count: HashMap::new(),
            writer_word_pairs: HashMap::new(),
            writer_word_probs: HashMap::new(),
            lines_enc: lines_encoded,
            lines: data,
        };

        return ngram;
    }

    fn fill_ngram(replique: &Vec<ReplyLine>, ngram: &mut Ngram) {
        for (line, dialog) in replique.iter().zip(ngram.lines_enc.iter()) {
            // Statistics how many lines each pony has said something in this program
            let p = ngram.ponys_line_count.entry(line.pony.to_owned()).or_insert(0);
            let w = ngram.writers_line_count.entry(line.writer.to_owned()).or_insert(0);
            *p += 1;
            *w += 1;

            // Word pairs by pony
            let pony_word_pairs = ngram.pony_word_pairs.entry(
                line.pony.to_owned()).or_insert(HashMap::new());
            let pony_word_probs = ngram.pony_word_probs.entry(
                line.pony.to_owned()).or_insert(HashMap::new());
            let mut id_old = dialog.first().unwrap().clone();
            let count1s = pony_word_probs.entry(id_old).or_insert(0);
            *count1s += 1;
            for &id_new in dialog.iter().skip(1) {
                let count1s = pony_word_probs.entry(id_new).or_insert(0);
                let count2s = pony_word_pairs.entry((id_old, id_new)).or_insert(0);
                *count1s += 1;
                *count2s += 1;
                id_old = id_new;
            }

            // Word pairs by writer
            let writer_word_pairs = ngram.writer_word_pairs.entry(
                line.writer.to_owned()).or_insert(HashMap::new());
            let writer_word_probs = ngram.writer_word_probs.entry(
                line.writer.to_owned()).or_insert(HashMap::new());
            let mut id_old = dialog.first().unwrap().clone();
            let count1s = writer_word_probs.entry(id_old).or_insert(0);
            *count1s += 1;
            for &id_new in dialog.iter().skip(1) {
                let count1s = writer_word_probs.entry(id_new).or_insert(0);
                let count2s = writer_word_pairs.entry((id_old, id_new)).or_insert(0);
                *count1s += 1;
                *count2s += 1;
                id_old = id_new;
            }

            // Said words by pony
            let words_by_pony = ngram.ponys_word_count.entry(
                line.pony.to_owned()).or_insert(0);
            *words_by_pony += dialog.len();

            // Said words ny anypony written by writer
            let words_by_writer = ngram.writers_word_count.entry(
                line.writer.to_owned()).or_insert(0);
            *words_by_writer += dialog.len();
        }
    }

    /// Copy vocabulary from train set to test set
    pub fn transform_vocabulary(ngram_from: &Ngram, ngram_to: &mut Ngram) {

        // Oh yes! I was reading offical documentation for hash map entry
        // and then saw this line:
        //      map.entry("poneyland").or_insert(3);
        // https://doc.rust-lang.org/std/collections/hash_map/enum.Entry.html
        // Ponies for the win.

        let mut old_to_new_num = HashMap::new();
        for (&num_old, name) in ngram_to.map_nums_to_names.iter() {
            match ngram_from.map_names_to_nums.get(name) {
                Some(num_new) =>
                    {old_to_new_num.insert(num_old, num_new);} ,
                None => {continue;},
            }
        }
        ngram_to.map_names_to_nums = ngram_from.map_names_to_nums.clone();
        ngram_to.map_nums_to_names = ngram_from.map_nums_to_names.clone();


        let old_map_nums_to_count = ngram_to.map_nums_to_count.clone();
        ngram_to.map_nums_to_count = ngram_from.map_nums_to_count.clone();
        for (_n, v) in ngram_to.map_nums_to_count.iter_mut() {
            *v = 0;
        }
        for (num_o, count_o) in old_map_nums_to_count.iter() {
            match old_to_new_num.get(num_o) {
                Some(num_n) => {
                    let m = ngram_to.map_nums_to_count.get_mut(&num_n)
                            .unwrap();
                    *m = *count_o;},
                None => {continue;},
            }
        }
        // Too lazy, I just diasble all other fields on other ngram, so I
        // do not need to do num-word transformation on the other data
        // structures.
        ngram_to.pony_word_pairs = HashMap::new();
        ngram_to.writer_word_pairs = HashMap::new();
        for line in ngram_to.lines_enc.iter_mut() {
            for num_old in line.iter_mut() {
                *num_old = match old_to_new_num.get(num_old) {
                    Some(num_new) => **num_new,
                    None => 0,
                }
            }
        }
    }

    fn print_info(ngram: &Ngram) {

        println!("Top 100 words in the show. Total word count: {}", ngram.total_word_count);
        let counts: Vec<&usize> = ngram.map_nums_to_count.values().collect();
        let nums: Vec<&usize> = ngram.map_nums_to_count.keys().collect();
        let perm = permutation::sort(&counts[..]);
        let ordered_nums = perm.apply_slice(&nums[..]);

        for num in ordered_nums.iter().rev().take(100) {
            let word = ngram.map_nums_to_names[num].clone();
            let count = ngram.map_nums_to_count[num];
            println!("    Prob: {:.5},  Count: {:5},  - {:16}",
                     count as f64 / ngram.total_word_count as f64, count, word);
        }
        // Search occurence of words "magic" and "friendship"
        println!("--");
        for &word in ["magic", "friendship"].iter() {
            let num = ngram.map_names_to_nums[word];
            let count = ngram.map_nums_to_count[&num];
            println!("    Prob: {:.5},  Count: {:5},  - {:16}",
                     count as f64 / ngram.total_word_count as f64, count, word);
        }

        let characters = ngram.ponys_word_count.len();
        println!("\n\nTop 50 characters with highest word count. Total {} characters.", characters);
        let mut ponies = Vec::with_capacity(characters);
        let mut ponys_words = Vec::with_capacity(characters);
        for (pony, count) in ngram.ponys_word_count.iter() {
            ponies.push(pony.to_owned());
            ponys_words.push(*count);
        }
        let perm = permutation::sort(&ponys_words[..]);
        let ordered_ponies = perm.apply_slice(&ponies[..]);
        let ordered_words = perm.apply_slice(&ponys_words[..]);

        for (pony, count) in izip!(ordered_ponies.iter(), ordered_words.iter())
                .rev().take(50) {
            println!("    Count: {:5},  - {}", count, pony);
        }

        let num_writers = ngram.writers_word_count.len();
        println!("\n\nTop 50 writers with highest word count. Total {} writers.", num_writers);
        let mut writers = Vec::with_capacity(num_writers);
        let mut writer_count = Vec::with_capacity(num_writers);
        for (writer, count) in ngram.writers_word_count.iter() {
            writers.push(writer.to_owned());
            writer_count.push(*count);
        }
        let perm = permutation::sort(&writer_count[..]);
        let ordered_writers = perm.apply_slice(&writers[..]);
        let ordered_counts = perm.apply_slice(&writer_count[..]);

        for (pony, count) in izip!(ordered_writers.iter(), ordered_counts.iter())
                .rev().take(50) {
            println!("    Count: {:5},  - {}", count, pony);
        }
    }

}

fn split_train_test<T>(vec: Vec<T>, precentage: f64) -> (Vec<T>, Vec<T>){
    assert!(0.0 <= precentage && precentage <= 1.0);
    let N = vec.len() as usize;
    let train_size = (N as f64 * precentage) as usize;
    let mut ids: Vec<usize> = (0..N).collect();

    ids.shuffle(&mut rand::thread_rng());

    let mut train = Vec::<T>::new();
    train.reserve(train_size);
    let mut test = Vec::<T>::new();
    test.reserve(N-train_size);

    for (val, &id) in vec.into_iter().zip(ids.iter()) {
        if id < train_size {
            train.push(val);
        } else {
            test.push(val);
        }
    }

    return (train, test);
}


mod evaluation {
    use super::*;
    pub fn eval_ngrams(train_ngram: &Ngram, test_ngram: &Ngram) {
        use std::cmp::{min};
//        struct Result {
//            pony: String,
//            accuracy: f64,
//            line_count: usize,
//        }
//        let results = Vec::<Result>::new();

        let mut total_count = 0;
        let mut got_right_pony = 0;
        let mut got_right_writer = 0;
        let mut got_in_top_5_ponies = 0;
        let mut got_in_top_5_writers = 0;

        println!("\n\n\n Next some few random lines and the ponies & writers predicted.");
        println!("P_log means logarithmic probability.");

        for (encline_te, field_te) in test_ngram.lines_enc.iter()
                .zip(test_ngram.lines.iter()) {

            if encline_te.len() < 20 { continue; }  // Too short phrase to analyze
            let (P_ponies, P_writers)
                    = match predict_pony_and_writer(&train_ngram, &encline_te, ) {
                Some(v) => v,
                None => continue,  // Too short phrase to analyze
            };

            total_count += 1;

            if P_ponies[0].0 == field_te.pony {
                got_right_pony += 1
            }
            if P_writers[0].0 == field_te.writer {
                got_right_writer += 1
            }
            if P_ponies.iter().take(5).any(|x| x.0 == field_te.pony) {
                got_in_top_5_ponies += 1;
            }
            if P_writers.iter().take(5).any(|x| x.0 == field_te.writer) {
                got_in_top_5_writers += 1;
            }

            // Print first few examples
            if total_count < 10 {
                println!("\n-----------------------------------------------------\n");
                println!("Epsiode: {}", field_te.title);
                println!("Pony: {}", field_te.pony);
                println!("Writer: {}", field_te.writer);
                println!("Phrase: \"{}\"", field_te.dialog);
                println!("Most probable ponies:");
                for i in 0..min(P_ponies.len(), 10) {
                    println!("    Rank: {:2}. P_log:{:.1}, {}", i + 1, P_ponies[i].1, P_ponies[i].0);
                }
                if true {
                    println!("Most probable writers:");
                    for i in 0..min(P_writers.len(), 10) {
                        println!("    Rank: {:2}. P_log:{:.1}, {}", i + 1, P_writers[i].1, P_writers[i].0);
                    }
                }
            }
        }
        println!("\n-----------------------------------------------------\n");
        println!("\n\nSummary:\n");
        println!("Predicted right pony:            {} / {} = {:.2}",
                 got_right_pony, total_count,
                 got_right_pony as f64 / total_count as f64);
        println!("Predicted right writer:          {} / {} = {:.2}",
                 got_right_writer, total_count,
                 got_right_writer as f64 / total_count as f64);
        println!("Predicted right pony in top 5:   {} / {} = {:.2}",
                 got_in_top_5_ponies, total_count,
                 got_in_top_5_ponies as f64 / total_count as f64);
        println!("Predicted right writer in top 5: {} / {} = {:.2}",
                 got_in_top_5_writers, total_count,
                 got_in_top_5_writers as f64 / total_count as f64);
    }

    fn predict_pony_and_writer(train: &Ngram, test_line: &Vec<usize>)
        -> Option<(Vec<(String, f64)>, Vec<(String, f64)>)>{

        // Yay, ponies

        type A = HashMap<String, HashMap<usize, usize>>;
        type B = HashMap<String, HashMap<(usize, usize), usize>>;
        type C = HashMap<String, usize>;
        let calc_log_P =
                |singles: &A, pairs: &B, word_count: &C, print: bool|
                    -> Option<Vec<(String, f64)>> {
            let n_size = 2;
            assert!(test_line.len() >= n_size);
            let mut Ps = Vec::<(String, f64)>::with_capacity(pairs.len());

            // key is either pony or writer
            for (key, pair_table) in pairs.iter() {
                let N = word_count[key];
                let single_table = &singles[key];
                let P_not_found = 1.0 / (10000000*N + 2) as f64;
                let mut occurences = 0;
                //let N = train.total_word_count;
                // Some init value, logarithm cannot be taken from P=0
                let mut log_P = (1.0 / (1*N + 2) as f64).log2();
                if print {println!("Key {}", key);}
                for id in (n_size - 1)..(test_line.len()) {  // word_id
                    match pair_table.get(&(test_line[id-1], test_line[id])) {
                        Some(count2s) => {
                            if print {print!("    1n:{}", count2s);}
                            occurences += 1;
                            log_P += (*count2s as f64 / (N-1) as f64).log2();
                        }, None => {
                            if print {print!("    None");}
                            match single_table.get(&test_line[id]){
                                Some(count1s) => {
                                    let mean_level =
                                            train.map_nums_to_count[&test_line[id]] as f64
                                                    / train.total_word_count as f64;
                                    let current_level = *count1s as f64 / N as f64;
                                    let ratio = current_level / mean_level;
                                    if print { print!("    2n:{}!", count1s); }
                                    occurences += 1;
                                    log_P += (P_not_found*100.0*ratio).log2();
                                }
                                None => {
                                    log_P += P_not_found.log2();
                                }
                            }
                        },
                    }
                    if print {println!(" -- {} {}",
                                       train.map_nums_to_names[&test_line[id-1]],
                                       train.map_nums_to_names[&test_line[id]]);}
                }
                if occurences < 1 {
                    log_P = -100000.0;
                }
                else if occurences < 3 {
                    log_P = -10000.0;
                }
                else if occurences < 5 {
                    log_P = -1000.0;
                }
                Ps.push((key.clone(), log_P));
            }
            Ps.sort_unstable_by(
                |&(_,a), &(_,b)| b.partial_cmp(&a).unwrap());

            if Ps[2].1 > -999.9 {
                return Some(Ps);  // Enough data
            } else {
                return None;  // Not enough data
            }
        };

        let P_ponies = calc_log_P(
            &train.pony_word_probs,
            &train.pony_word_pairs,
            &train.ponys_word_count, false);
        let P_writers = calc_log_P(
            &train.writer_word_probs,
            &train.writer_word_pairs,
            &train.writers_word_count, false);

        match (P_ponies, P_writers) {
            (Some(p), Some(w))=> {
                return Some((p, w));},
            _ => {
                return None;}  // Not enough data to give good answer
        }
    }
}




fn main() {
    let train_test_fraction = 0.96;

    let all_dialogs = parse_clean_dialog_csv().unwrap();
    let (train_dialogs, test_dialogs)
            = split_train_test(all_dialogs, train_test_fraction);

    let train_ngram = ngram_stuff::create_ngram(&train_dialogs, true);
    let mut test_ngram = ngram_stuff::create_ngram(&test_dialogs, false);
    // Copy vocabulary from train set to test set
    ngram_stuff::transform_vocabulary(&train_ngram, &mut test_ngram);

    evaluation::eval_ngrams(&train_ngram, &test_ngram);

}


// https://www.reddit.com/r/mylittlepony/comments/jj53v/mlp_ascii_art/
//
//                 .:rLfth1FUtYJ7ri:.
//                ::iiri77jthXbbDbZDRDR9Fci
//                            .:L1f1fhFXPEbDPj:
//                    .:LjhhpXXf1ffUfUff1fh1P9Z0t,
//                .71MMRDD99XphXhh2F21f1f1f1212SpZSr
//              rPDpXttJjYJYjYjJjjtUft2fF2Ffh2F1hh9E9i
//            iEDPfhtJcJJYLY7L777L777L7L7cLJJUJ1fF1P9RP:
//          .PQR0FY:.JPUJLLLc7c7c7L7c7L7L7L777L7LLYcU20M2
//         YQM7:    71cL7L7c7L7cLcLcLYLcLcLcLcLcLL7L7ccjXM:
//        :L.     U017L7cLcLcLYLYLc7YLcLYLcLYcjj2th1F2FfFPQF
//              iMXJLLLcLcLc7cLcLcLYLcLYcJjh1hjYri::,:,::irbQ.
//            1RUcLc7cLc7cLc7cLcLcLJYt2StYi:               JQi
//          iD9cLLcLYLYLcLcLcLcLYct11Jr.            . .     rQ7       .:i.
//         .SZ2Lc7cLcLcLcLcLcLccjf1c:           .   .   . .   :Qj   :LfjYc9:
//       ,fMhJ7LLc7cLcLc7cLL7cJtL:           .             .    ZUrFJr,,..7h
//..::rJ0DXYYLL7cLc7L777L7LLJY7,  .:7Uj:                        cj7:..:,:.:2:
//.:LXZRME00ppXPhhfJ777JYJ2XhSYJJc7bEi     .,i7YJ2t1ffJJ7r:.  rUi..,,:,:,,.Yr
//    ,.:rricr77hpQpQQ.,:,,. .U:  .icJUYc7r:i:iirir7YjFhXL7,,,:,:,:,:,.rL
//                .fQQQ :Q ..,. :Er:Jhcr::rcYYtSJr   ...   :r:.:,:,:,:,:,:.77
//               .hQQQ, 7Q .:,.rQ9t7. :FQQQQQE:.YEQQ7   iEr.,,:,:,:,:.i::,.77
//               FQQQQ  QX ,,.rpr.  7QQQQQQQQQQ,   7QQUQQp.,,:,:,:,:,:r,,:.Ui
//              cRQQQQQjQ .,:,i.  .QQQQQQQQXQQQQ7    LQh  ....:,:,:,,7:,:.:1.
//             .hRQQ0PQQ: ,:,:.. iQQQQQQQf   .QQQ     :Q .:rrr,:,:,,rr.:..LU
//             r::QQfhQr .:,:,, :QQQQQQQQ     rQQ,     QQQQQMJ,,:.,77.:,.:F,
//            .: rQQQQi .:,:,:. QQQQQQQQ,     :QQ:     Pp    ..,,:7r.,,.,Yt:
//        .j2Yf:iQQQQ, .,,:,:, :QQQQQQQQL     XQQ      MQLcr:.,,:7i.,,,.cLF:
//        LF:::ii:.  .,:,:,:,: fQQQQQ  QQ.   cQQL      rbQQQ2,,:i:.:,,,c7L2
//        cc..,,,...,.:,:,:,:, 2QQQQQ.,QQQ0SQQQZ       .    .,:,,,:,.,Y77F.
//        r1.,,:ri,,.:,:,:,:,, :QQQQQQQ0.JEQRQZ       .:,,,,,:,:,:..:cr71:
//         fJ...,.,.:r,,:,:,:,, tQQQQRMt  rFPi       .i::,:,:,:,,.,i17L2.  .t
//          Y2r:,. ,jt.,,:,:,:,..RQRRQQQM. .        ,i::,:,:,:...i7F9fJ   :PL
//           ,77LYjpQj,.:,:,:,:...iL1X9Fj         .:i::,:,:,,.:7UccXEr   7S:.
//               QQQQJ,,,:,:,:,:,.              .:::::,:,:,:..FDY7JMi   7h:.,
//               LQZQ7,.:,:,:,:,:,:::..    ..,::i::::,:,:,:, 7ZJ7rhF   iS:...
//               :QQXr.,,:,:,:,:,:,:,::i:i:i::::,:,:,:,:,:,..pt7r7Pc  .S:.:rr
//               ,Qtri.,:,:,:,:,:,:,:::,:,:,:::,:,:,:,:,,. :p27rrcDi  friL7ir
//               iQ7L:.:,:,:,:,:,:,:,:,:,:,:,:,:,:,:,,...,Y0j777LhXc:LJcr:...
//               tRr:.,,:,:,:,:,:,:,:,:,:,:,:,,,,.....,:UXFL77771fSr7cc:..:,:
//             .LXc:...,,:,:,:,:,:,:,:,:,:,:,:,..,,i7t1XtL777772f22irc,.,:,.r
//              ,:7JUL7ii::,,.,.,.,.,,:,:,:,:r1pDbbS1YLr77L77LFtfftiY:.,:,.:U
//                  .:i7LJYJcc7Lrri:::::,:,:,,.:rJt2JUjULL77Y1UfUFJ7r.,:,,,Jr
//                           ...,.rti::,:,:,:,,..   rh1c7rLUhUtjf2Jr,.:,:,c7.
