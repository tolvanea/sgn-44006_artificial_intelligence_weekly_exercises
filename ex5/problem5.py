import math

def nCr(n,k):
    return math.factorial(n) / (math.factorial(k)*math.factorial(n-k)) 

def interrupt(n,m,N):
    sum = 0.0
    for i in range(N-m):
        sum += 0.5**(N-n-1) * 0.5**i * nCr(N-n-1+i,i)
    return sum * 0.5

def main():
    N = 7
    scores = ((6,6), (5,6), (6,5), (3,3), (1,1), (4,2))
    for (n,m) in scores:
        print("{}–{}, P = {:.3f}".format(n, m, interrupt(n,m,N)))
        
        
main()
