#[allow(unused_imports)]
use failure::Error;

use ndarray as nd;
#[allow(unused_imports)] // Traits
use nd::prelude::{Dimension, NdFloat, ShapeBuilder, AsArray, };
use nd::{s};  // slice syntax

use rand::{thread_rng, seq::SliceRandom, Rng, rngs::StdRng, SeedableRng};
use std::time::Instant;


const DEBUG: bool = false;

struct Sudoku {
    board_solution: nd::Array4<i32>,
    board_partially_filled: nd::Array4<i32>,
}

impl Sudoku {
    fn new(n: usize, hardness: i32, seed: u64) -> Sudoku {
        assert!(hardness >= 1 && hardness <= 5);
        assert!(n >= 2);

        let mut solution = nd::Array4::<i32>::zeros((n, n, n, n));
        let mut rng = SeedableRng::seed_from_u64(seed as u64);

        // fill full board
        let _ = Sudoku::recursive_solution(0, &mut solution, true, true, Some(&mut rng));

        // Initialize partially filled board as full board and then remove numbers
        let mut partially_filled = solution.clone();

        // Remove values from filled one
        Sudoku::remove_numbers(hardness-1,&mut partially_filled, &mut rng);

        let sudoku = Sudoku {
            board_solution: solution,
            board_partially_filled: partially_filled,
        };
        return sudoku;
    }

    fn get_solution_board(&self) -> nd::Array4<i32>{
        return self.board_solution.clone();
    }

    fn get_partially_filled_board(&self) -> nd::Array4<i32>{
        return self.board_partially_filled.clone();
    }

    fn remove_numbers(hardness: i32, mut board: &mut nd::Array4<i32>,
                      rng: &mut StdRng){
        let n = board.dim().0;
        let mut threshold = 2_i32.pow(hardness as u32);
        while threshold > 0 {
            let mut indices = (9,9,9,9);
            let mut orig= 0;
            while orig < 1{
                let random_index = rng.gen_range(0, n.pow(4));
                indices = Sudoku::to_flat_itr(random_index, n);
                orig = board[indices];
            }
            board[indices] = 0;
            let count = Sudoku::recursive_solution(0, &mut board, false, false, Some(rng));

            assert!(count > 0);
            if count > 1 {
                if DEBUG{
                    println!("Value removing progress {}/{}",
                             threshold, 2_i32.pow(hardness as u32));
                }
                threshold -= 1;
                board[indices] = orig;
            }
        }
    }

    fn to_flat_itr(flat_itr: usize, n: usize) -> (usize, usize, usize, usize) {
        let i = flat_itr / n.pow(3);
        let j = (flat_itr % n.pow(3)) / n.pow(2);
        let k = (flat_itr % n.pow(2)) / n;
        let l = flat_itr % n;
        return (i,j,k,l);
    }

    fn test_direct_validity_of_placement(board: &nd::Array4<i32>,
                                         num: i32, i: usize, j: usize,
                                         k: usize, l: usize) -> bool{

        assert!(board[[i, j, k, l]] < 1);

        // Column
        for &v in board.slice(s![..,j,..,l]).iter(){
            if v == num {
                return false;
            }
        }
        // Row
        for &v in board.slice(s![i,..,k,..]).iter(){
            if v == num {
                return false;
            }
        }
        // Block
        for &v in board.slice(s![i,j,..,..]).iter(){
            if v == num {
                return false;
            }
        }
        return true;
    }

    fn debug_board(board: &nd::Array4<i32>, debug: bool, i: usize,
                       j: usize, k: usize, l: usize){
        for (_j, a3) in board.axis_iter(nd::Axis(1)).enumerate(){
            for (_l, a2) in a3.axis_iter(nd::Axis(2)).enumerate(){
                for (_i, a1) in a2.outer_iter().enumerate(){
                    for (_k, &a0) in a1.iter().enumerate(){
                        if debug && (i,j,k,l) == (_i,_j,_k,_l){
                            print!("{:2}#", a0);
                        }
                        else if a0 < 1{
                            print!("   ");
                        }
                        else {
                            print!("{:2} ", a0);
                        }
                    }
                    print!("| ");
                }
                println!("");
            }
            for (_i, a3) in board.axis_iter(nd::Axis(1)).enumerate() {
                for (_j, _a2) in a3.axis_iter(nd::Axis(2)).enumerate(){
                    print!("–––");

                }
                print!(" –");
            }
            println!("");
        }
        println!("");
    }

    fn print_board(board: &nd::Array4<i32>){
        Sudoku::debug_board(&board, false, 0, 0, 0, 0);
    }

    fn recursive_solution(flat_itr: usize, mut board: &mut nd::Array4<i32>,
                          leave_filled: bool, short_circuit: bool,
                          rng: Option<&mut StdRng>) -> usize{
        // Initialize stuff
        assert!(board.dim().0 == board.dim().1);
        assert!(board.dim().1 == board.dim().2);
        assert!(board.dim().2 == board.dim().3);
        assert!(!(!leave_filled && short_circuit));

        if DEBUG && leave_filled==true {
            for _i in 0..flat_itr{
                print!("-");
            }
            println!("");
        }

        let n = board.dim().0;
        let mut tmp;
        // let a: &mut StdRng = &mut SeedableRng::seed_from_u64(thread_rng().gen::<u64>());
        let rng2: &mut StdRng =
            match rng{
                Some(v) => v ,
                None => {
                    tmp = SeedableRng::seed_from_u64(thread_rng().gen::<u64>());
                    &mut tmp
                },
            };

        let (i,j,k,l) = Sudoku::to_flat_itr(flat_itr, n);

        // End condition
        if flat_itr == n.pow(4){
            return 1;
        }

        let mut count = 0;  // Number of solutions for current square

        let mut trials = (1..=(n.pow(2) as i32)).collect::<Vec<i32>>();
        trials.shuffle(rng2);
        let trials = nd::Array::from_vec(trials);

        // Value already filled
        if board[[i,j,k,l]] > 0{
            count += Sudoku::recursive_solution(flat_itr+1, &mut board,
                                        leave_filled, short_circuit, Some(rng2));
            return count;
        }
        else{
            for trial_idx in 0..(n.pow(2)){
                let trial = trials[trial_idx];
                // Test direct contradictions whether number can be placed here.
                match Sudoku::test_direct_validity_of_placement(&mut board,
                                                                     trial, i, j, k, l) {
                    false => {continue;},
                    true => {board[[i, j, k, l]] = trial;},
                }
                // Test indirectly whether this number causes zero solutions
                count += Sudoku::recursive_solution(flat_itr + 1, &mut board,
                          leave_filled, short_circuit, Some(rng2));
                if (count == 0) || (!leave_filled) {
                    board[[i,j,k,l]] = 0;
                }
                if (count != 0) && (leave_filled) {
                    return count
                }
            }
        }
        return count;  // Contradiction, some guess previously is wrong
    }
}



fn main() {
    // Let us consider a generalized version of Sudoku where the dimension of the
    // table is given as a parameter n. Standard Sudoku has n=3.


    // 2. Develop a program (use your favorite programming language) that generates
    // Sudoku quizzes to be solved given the parameter value . The program must
    // (in principle) be able to generate any quiz. What is the leading principle of
    // your program?

    // 3. Give the program another input; difficulty reaching from one star (easy) to
    // five stars (quite demanding). Do you manage to generate quizzes of different
    // difficulty?

    let seed = 42;
    let sudoku_1_star = Sudoku::new(3, 1, seed);
    let sudoku_5_stars = Sudoku::new(3, 5, seed);

    println!("Solution 3x3:");
    Sudoku::print_board(&sudoku_1_star.get_solution_board());

    println!("One star 3x3:");
    Sudoku::print_board(&sudoku_1_star.get_partially_filled_board());

    println!("Five stars  3x3:");
    Sudoku::print_board(&sudoku_5_stars.get_partially_filled_board());
    println!("");



    // 4. Develop a program that solves Sudoku quizzes of any dimension.

    // 5. Evaluate by empirical tests weather your quiz generator really achieves the
    // separation between difficulty classes.

    let mut board_1s = sudoku_1_star.get_partially_filled_board();
    let mut board_5s = sudoku_5_stars.get_partially_filled_board();


    let now_1s = Instant::now();
    let sol_count_1s = Sudoku::recursive_solution(0, &mut board_1s, true, false, None);
    let time_1s = now_1s.elapsed().as_micros();

    let now_5s = Instant::now();
    let sol_count_5s = Sudoku::recursive_solution(0, &mut board_5s, true, false, None);
    let time_5s = now_5s.elapsed().as_micros();

    // Be sure that there exists only one solution.
    assert!(sol_count_1s == 1);
    assert!(sol_count_5s == 1);

    println!("Solution 1 star, took {} ms", time_1s as f64 / 1000.0);
    Sudoku::print_board(&board_1s);
    println!("Solution 5 stars, took {} ms", time_5s as f64 / 1000.0);
    Sudoku::print_board(&board_5s);
    println!("");


    // 6. How do your programs behave with respect to increasing value of the
    // parameter ? What is the largest for which you can solve quizzes in a
    // reasonable time?
    // Answer: it gets a lot more computatioally demanding.
    println!("Three stars  5x5:");
    let sudoku_5x5 = Sudoku::new(5, 2, seed);
    let mut board_5x5 = sudoku_5x5.board_partially_filled.clone();
    //Sudoku::print_board(&sudoku_5x5.get_solution_board());
    Sudoku::print_board(&sudoku_5x5.get_partially_filled_board());
    let now_5x5 = Instant::now();
    let sol_count_5x5 = Sudoku::recursive_solution(0, &mut board_5x5, true, false, None);
    let time_5x5 = now_5x5.elapsed().as_millis();
    println!("Two star  5x5:, took {} ms", time_5x5 as f64 / 1000.0);
    Sudoku::print_board(&board_5x5);

    assert!(sol_count_5x5 == 1);




}


