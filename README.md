# SGN-44006 Artificial Intelligence
Tampere University

## Exercise 1

Sudoku board creation and solver with brute force depth-first-search (i.e. back propagation). Supports sudoku boards with also other sizes than 9*9 = (3*3)*(3*3).
    
## Exercise 2

Tic-tac-toe solver with brute force depth-first-search. Grid sizes >3 are supported but not feasible due to the computational complexity.
    
## Exercise 3

Sudoku solver with constraint satisfaction approach (arch-consistency, AC3). To be honest, brute force DFS was way more efficient with 9*9 and 16*16 board.

## Exercise 4

Classify text data with ngram (1-gram and 2-gram). Analyze phrase lines of a (good) TV-series, and try to predict who would say given phrase.

## Exercise 5

Just pen and paper probability problems, no coding :( .
